__author__ = "Simon Melotte"

import os
import requests
import sys
import json
import re
from concurrent.futures import ThreadPoolExecutor, as_completed
from pprint import pprint


def createDirectory(directory):
    try: 
        if not os.path.exists(directory):
            os.mkdir(directory)
    except OSError:  
        print ("Creation of the directory {} failed".format(directory) )
    else:  
        print ("Successfully created the directory {} ".format(directory) )


def download_url(url, session, filename):

    byte = 0

    try:
        if os.path.isfile(filename):
            byte = os.path.getsize(filename)
            print ("== File was found: {} byte= {}".format(session, str(byte) ) )

        if not os.path.isfile(filename) or byte < 30000:
            print ("==== Start downloading: {}".format(session))
            r = requests.get(url)
            if r.status_code == 200:
                print ("==== File was downloaded: {}".format(session))
                with open(filename , "wb") as f:  
                    f.write(r.content)
                    byte = len(r.content)
            else:
                print ("==== URL was not found for: {}".format(session))
                return None
        else:
            print ("=== File already exists and size is bigger than one page - Filename = {}".format(filename))
            return byte
    except Exception as e:
        print ("=============== Exception for {} {}".format(session, e))
        return None

    return byte


def getPresentations(baseurl, sessionfile, directory, workers):
    with open(sessionfile) as json_file:  
        data = json.load(json_file)
        # Remove some special characters and the last dot
        rex = re.compile(r"([^\w,\-'\.()]+|\.$)")

        NOT_FOUND = []

        # Basic threading for HTTP download speed improvements
        with ThreadPoolExecutor(max_workers=workers) as executor:
            tasks = {}
            for item in data: 
                item_name = item["name"].rstrip()
                item_desc = re.sub(r'&', 'and', item['description'])
                item_desc = rex.sub(" ", item_desc)
                item_desc = item_desc.rstrip()
                pdf_name = f'{item_name}.pdf'
                file_name = f'{item_name} - {item_desc}.pdf'
                print("Evaluating followig file: {} - description = {}".format(item_name, item_desc) )           

                url = baseurl + pdf_name
                print ("= URL fullpath is: {}".format(url) )
                
                fullpath = os.path.join(directory, file_name)

                tasks[executor.submit(download_url, url, pdf_name, fullpath)] = url
            
            for future in as_completed(tasks):
                url = tasks[future]

                try:
                    data = future.result()
                except Exception as e:
                    print(f'Exception: {e}')
                else:
                    if data:
                        print(f'Downloaded {url}, size: {data}')
                    else:
                        NOT_FOUND.append(url)

        # Quick and dirty output errors
        print("NOT FOUND: {}\n--------------------------------------------".format(len(NOT_FOUND)))
        pprint(NOT_FOUND)


def main():    
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    PDFS =  os.path.join(THIS_FOLDER, "pdfs-2020")
    BASEURL = "https://www.ciscolive.com/c/dam/r/ciscolive/emea/docs/2020/pdf/"

    sessionfile = os.path.join(THIS_FOLDER, "cleur-sessions.2020.json")
    createDirectory(PDFS)
    getPresentations(BASEURL, sessionfile, PDFS, workers=5)

if __name__ == "__main__":
    main()
